from collections import namedtuple
from itertools import repeat
from scipy.sparse import csr_matrix
from numpy import array

class Hash(object):

    def __init__(self, size):
        self._size = size

    def __call__(self, obj):
        return hash(obj) % self._size


Batch = namedtuple('Batch', ('y', 'X'))


class Iterator:

    def __init__(self, file_path, batch_size=1, hash_size=2*12):
        self._batch_size = batch_size
        self._hash_size = hash_size
        self._in = open(file_path)
        self._hash = Hash(hash_size)

    def __iter__(self):
        return self

    def next(self):
        labels = []
        row_index, col_index, data = [], [], []
        for i in range(self._batch_size):
            try:
                iterable = iter(next(self._in).split())
                labels.append(float(next(iterable)))

                for pair in iterable:
                    name, value = pair.split(':')
                    row_index.append(i)
                    col_index.append(self._hash(name))
                    data.append(float(value))
            except StopIteration:
                if not labels:
                    raise
            
        return Batch(
            array(labels),
            csr_matrix(
                (data, (row_index, col_index)),
                shape=(len(labels), self._hash_size),
                dtype=float
            )
        )

    def __enter__(sefl):
        pass

    def __exit__(sefl):
        self._in.close()

