data=$1
STREAMING_JAR=/opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar
hadoop jar $STREAMING_JAR \
-libjars $STREAMING_JAR \
-Dmapred.job.queue.name=single \
-Dmapred.job.name="numerical stats calculation" \
-Dmapred.reduce.tasks=10 \
-file stats_calc_mapper.py \
-file stats_calc_reducer.py \
-file ../../data/config.conf \
-mapper "./stats_calc_mapper.py config.conf" \
-reducer "./stats_calc_reducer.py" \
-input $data \
-output stats
