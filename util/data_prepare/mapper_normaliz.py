import sys

filename_stat = sys.argv[1]

stat_dict = {}
with open(filename_stat) as stat:
    for line in stat:
        key, tmp = line.rstrip('\n').split('\t')
        mean, std = tmp.split(',')
        stat_dict[key] = {}
        stat_dict[key]["mean"] = float(mean)
        stat_dict[key]["std"] = float(std)

filename_config = sys.argv[2]

config_dict = {}
with open(filename_config) as config:
    for line in config:
        line = line.rstrip('\n').split('\t')
        for ind in line[1].split(','):
            config_dict[int(ind)] = line[0]

for line in sys.stdin:
    key, value = line.rstrip('\n').split('\t')
    output = []
    for ind, val in enumerate(value.split(',')):
        if config_dict[ind] == "categorical":
            if val != '':
                output.append("cat" + str(ind) + '_' + val + ':1')
        elif config_dict[ind] == "numerical":
            if val != '' and float(val) != 0:
                    output.append("num" + str(ind) + ':' + str((float(val) -\
                       stat_dict[ind]["mean"])/stat_dict[ind]["std"]))

    print key + '\t' + ','.join(output)
