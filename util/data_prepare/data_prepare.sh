cat  | tail -n +2 | sed 's/,/\t/g' > /hdfs/user/errorf/data

./stats_calc.sh tiny_data

for name in $(find /hdfs/user/errorf/stats -type f);
do
    cat $name >> /hdfs/user/errorf/means_stds
done

#rm -r /hdfs/user/errorf/stats
