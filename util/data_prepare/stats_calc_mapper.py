#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys

##############################################################################
if __name__ == '__main__':
    config_dict = {}
    config_filename = sys.argv[1]
    with open(config_filename) as config_file:
        for line in config_file:
            name, indices = line.strip().split('\t')
            config_dict[name] = [int(ind) for ind in indices.split(',')]

    is_first_line = True
    for line in sys.stdin:
        if is_first_line:
            is_first_line = False
            continue
        items = line.strip().split(',')
        label = items[0]
        items = items[1:]
        for index in config_dict['numerical']:
            if items[index] != '':
                print str(index) + '\t' + items[index] + ',' + label

##############################################################################
