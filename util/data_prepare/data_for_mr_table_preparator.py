# -*- coding: utf-8 -*-
import os
import glob
from time import time

##############################################################################

def train_table_builder(train_filenames, output_filename):
    start_time = time()
    with open(output_filename, 'w') as output:
        current_id = 0
        for train_filename in train_filenames:
            negative_objects = []
            positive_objects = []
            for line in open(train_filename):
                if line[0] == '0':
                    negative_objects.append('-1' + line[1:])
                elif line[0] == '1':
                    positive_objects.append(line)

                if len(negative_objects) + len(positive_objects) == 5000:
                    if len(negative_objects) == 0 or len(positive_objects) == 0:
                        negative_objects = []
                        positive_objects = []
                        continue
                    for neg_obj, pos_obj in zip(negative_objects,
                                                positive_objects):
                        output.write(str(current_id) + '\t' + neg_obj)
                        output.write(str(current_id) + '\t' + pos_obj)
                    
                    if len(negative_objects) < len(positive_objects):
                        for i in xrange(len(positive_objects) - len(negative_objects)):
                            output.write(str(current_id) + '\t' + positive_objects[i + len(negative_objects)])
                    elif len(negative_objects) > len(positive_objects):
                        for i in xrange(len(negative_objects) - len(positive_objects)):
                            output.write(str(current_id) + '\t' + negative_objects[i + len(positive_objects)])

                    negative_objects = []
                    positive_objects = []
                    current_id += 1

            print "%s is rewritten. Time left: %0.2f" % \
                                    (train_filename, (time() - start_time))
            os.remove(train_filename)

def main():
    train_filenames = glob.glob('../train_set/part-*')
    train_table_builder(sorted(train_filenames), 'train_table.tsv')

##############################################################################

if __name__ == '__main__':
    main()

##############################################################################
