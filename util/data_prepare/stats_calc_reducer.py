#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import math
##############################################################################
if __name__ == '__main__':
    values_sum = 0.0
    squared_values_sum = 0.0
    objects_counter = 0

    key_pred = ''

    for line in sys.stdin:
        key, value = line.strip().split('\t')
        value, label = map(float, value.split(','))
        if key_pred and key != key_pred:
            mean = values_sum / objects_counter
            numerator = (squared_values_sum - (mean ** 2) / objects_counter)
            std = math.sqrt(numerator / (objects_counter - 1))
            print key_pred + '\t' + str(mean) + ',' + str(std)
            values_sum = 0.0
            squared_values_sum = 0.0
            objects_counter = 0
        values_sum += value
        squared_values_sum += value ** 2
        objects_counter += 1
        key_pred = key

    mean = values_sum / objects_counter
    numerator = (squared_values_sum - (mean ** 2) / objects_counter)
    std = math.sqrt(numerator / (objects_counter - 1))

    print key_pred + '\t' + str(mean) + ',' + str(std)

##############################################################################
