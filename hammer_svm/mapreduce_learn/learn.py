# -*- coding: utf-8 -*-
#!/usr/bin/env python2.5

import sys
sys.path.append('../')
sys.path.append('/home/klunev/trunk/arcadia/yweb/scripts/datascripts/common/')
import iterator as it
import hammer_svm as hsvm
from time import time
from mapreducelib import Record
from mapreducelib import MapReduce as MR

##############################################################################

def trainOperation(key, recs):
    clf = hsvm.HammerSVM()
    negative_class = []
    positive_class = []
    for rec in recs:
        if rec.subkey == '-1':
            negative_class.append(rec.value)
        elif rec.subkey == '1':
            positive_class.append(rec.value)

    labels = []
    features = []
    for negative, positive in zip(negative_class, positive_class):
        features.append(negative)
        features.append(positive)
        labels.append('-1')
        labels.append('1')
    if len(negative_class) < len(positive_class):
        for positive in positive_class[len(negative_class):]:
            features.append(positive)
            labels.append('1')
    elif len(negative_class) > len(positive_class):
        for negative in negative_class[len(positive_class):]:
            features.append(negative)
            labels.append('-1')

    start = time()
    timeout = 60 * 10 # 10 minutes in seconds
    batch_size = 100
    iterator = it.Iterator(data=features, batch_size=batch_size)
    for batch_count, batch in enumerate(iterator):
        clf.partial_fit(batch, labels[batch_count * batch_size :
                       (batch_count + 1) * batch_size])
        if time() - start > timeout:
            break
    
    Xp, Xn = clf.centers()
    Xp_str = '\t'.join(map(lambda elem: "{}:{}".format(elem[0], elem[1]), \
                                                    zip(Xp.indices, Xp.data)))
    Xn_str = '\t'.join(map(lambda elem: "{}:{}".format(elem[0], elem[1]), \
                                                    zip(Xn.indices, Xn.data)))
    yield Record('Xp', str((batch_count + 1) * batch_size), Xp_str)
    yield Record('Xn', str((batch_count + 1) * batch_size), Xn_str)

##############################################################################

def main():
    MR.useDefaults(server='cedar00.search.yandex.net:8013', verbose=True, testMode=True)
    MR.runReduce(trainOperation, srcTable="romario/big_data/tiny_train",
                                 dstTable="romario/big_data/result_2",
                 files={'../hammer_svm.py': 'hammer_svm.py',
                        '/home/klunev/trunk/arcadia/yweb/scripts/datascripts/common/mapreducelib.py': 'mapreducelib.py',
                        '/home/klunev/lsml_ctr_prediction/hammer_svm/mapreduce_learn/iterator.py': 'iterator.py',
                        '/usr/lib/libblas/libblas.so': 'libblas.so'
                       },
                 cpuIntensive=1,
                 jobCount=4
                )

##############################################################################

if __name__ == "__main__":
    main()

##############################################################################
