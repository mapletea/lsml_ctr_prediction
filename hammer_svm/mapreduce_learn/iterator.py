from collections import namedtuple
from itertools import repeat
from scipy.sparse import csr_matrix
from numpy import array

class Hash(object):

    def __init__(self, size):
        self._size = size

    def __call__(self, obj):
        return hash(obj) % self._size

class Iterator:

    def __init__(self, data, batch_size=1, hash_size=2**22):
        self._batch_size = batch_size
        self._hash_size = hash_size
        self._in = data
        self._hash = Hash(hash_size)
        self._counter = 0

    def __iter__(self):
        return self

    def next(self):
        row_index, col_index, data = [], [], []
        cur_data = self._in[self._counter : self._counter + self._batch_size]
        if not len(cur_data):
            raise StopIteration
        try:
            for i, line in enumerate(cur_data):
                self._counter += 1
                factors = line.split('\t')

                for pair in factors:
                    name, value = pair.split(':')
                    row_index.append(i)
                    col_index.append(self._hash(name))
                    data.append(float(value))

        except StopIteration:
            pass
            

        return csr_matrix(
                (data, (row_index, col_index)),
                shape=(len(data), self._hash_size),
                dtype=float
            )

    def __enter__(self):
        pass

    def __exit__(self):
        self._in.close()
