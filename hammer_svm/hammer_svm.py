#! /usr/bin/python2.7
import numpy as np
from sklearn.base import BaseEstimator
import scipy.sparse
from scipy.sparse import csr_matrix


class HammerSVM(BaseEstimator):
    '''Hammer SVM classifier'''
    def __init__(self, epsilon=1e-14):
        self.p_weights = None
        self.n_weights = None
        self.Xp = None
        self.Xn = None
        self.Xp_Xp = None
        self.Xn_Xn = None
        self.Xn_Xp = None
        self.epsilon = epsilon

    def fit(self, X, y):
        # clear data, perform partial_fit
        return self

    def partial_fit(self, X, y):

        for i in range(X.shape[0]):
            if y[i] == 1:
                if self.p_weights is None:
                    self.p_weights = csr_matrix((1, 1))
                else:
                    self._expand_vector(self.p_weights, 1)
                if self.Xp is None:
                    self.Xp = X[i]
                    self.Xp_Xp = self._dot(self.Xp, self.Xp)
                    #inefficient!
                    self.p_weights[0] = 1.
                    if self.Xn is not None:
                        self.Xn_Xp = self._dot(self.Xp, self.Xn)
                    continue

            else:
                if self.n_weights is None:
                    self.n_weights = csr_matrix((1, 1))
                else:
                    #add "if self.Xp is not None and self.Xn is not None..." somewhere
                    self._expand_vector(self.n_weights, 1)

                if self.Xn is None:
                    self.Xn = X[i]
                    self.Xn_Xn = self._dot(self.Xn, self.Xn)
                    #inefficient!
                    self.n_weights[0] = 1.
                    if self.Xp is not None:
                        self.Xn_Xp = self._dot(self.Xp, self.Xn)
                    continue

            if self.Xn is not None and self.Xp is not None:
                self._update_weights(X[i], y[i])

        return self

    def _update_weights(self, xk, label, index=-1):
        Xp_xk = self._dot(self.Xp, xk)
        Xn_xk = self._dot(self.Xn, xk)
        xk_xk = self._dot(xk, xk)

        if label == 1:
            #print self.Xp_Xp, xk_xk, -2 * Xp_xk
            lambda_u = (self.Xp_Xp - self.Xn_Xp - Xp_xk + Xn_xk) /\
                (self.Xp_Xp + xk_xk - 2 * Xp_xk)
        else:
            #print self.Xn_Xn, xk_xk, -2 * Xn_xk
            lambda_u = (self.Xn_Xn - self.Xn_Xp - Xn_xk + Xp_xk) /\
                (self.Xn_Xn + xk_xk - 2 * Xn_xk)
        weights = self.p_weights if label == 1 else self.n_weights
        lambda_ = min(1, max(-weights[0, -1] / (1 - weights[0, -1]), lambda_u))

        # print
        # print 'before: ', weights.toarray()
        # print "Xp_xk: {}, Xn_xk: {}, xk_xk: {}".format(Xp_xk, Xn_xk, xk_xk)
        # print 'lambda: ', lambda_
        # print

        # TODO: we should be able to make updates by index (update X[i])
        last_val = weights[0, -1]
        weights *= 1 - lambda_

        weights[0, -1] += last_val + lambda_
        # print 'after : ', weights.toarray()

        # center updating

        if label == 1:
            self.Xp = (1 - lambda_) * self.Xp + lambda_ * xk
        else:
            self.Xn = (1 - lambda_) * self.Xn + lambda_ * xk

        # dot products updating

        if label == 1:
            self.Xp_Xp = ((1 - lambda_) ** 2) * self.Xp_Xp +\
                2 * lambda_ * (1 - lambda_) * Xp_xk +\
                (lambda_ ** 2) * xk_xk
            self.Xn_Xp = (1 - lambda_) * self.Xn_Xp + lambda_ * Xn_xk
        else:
            self.Xn_Xn = ((1 - lambda_) ** 2) * self.Xn_Xn +\
                2 * lambda_ * (1 - lambda_) * Xn_xk +\
                (lambda_ ** 2) * xk_xk
            self.Xn_Xp = (1 - lambda_) * self.Xn_Xp + lambda_ * Xp_xk

    def predict(self, X):
        return 2 * ((self.decision_function(X) -
                    self.epsilon >= 0).astype(int) - 0.5)

    def decision_function(self, X):
        return np.array((self.Xp - self.Xn).dot(X.transpose()) +
                        np.ones(X.shape[0]) * (self.Xn_Xn - self.Xp_Xp) / 2)

    def predict_proba(self, X):
        return self._sigmoid(self.decision_function(X))

    def get_params(self, deep=True):
        return {}

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            self.setattr(parameter, value)
        return self

    def set_Xp_Xn(self, Xp, Xn):
        self.Xp = Xp
        self.Xn = Xn

    def coef(self):
        return scipy.sparse.hstack((self.Xp-self.Xn,
                                    (self.Xn_Xn - self.Xp_Xp) / 2))

    def centers(self):
        return self.Xn, self.Xp

    def _sigmoid(self, X):
        return 1. / (1. + np.exp(-X))

    def _expand_vector(self, vector, count):
        #if vector is None:
            #return csr_matrix((1, count))
        #else:
            #return scipy.sparse.hstack([vector,
                                        #csr_matrix((1, count), dtype=float)],
                                       #format='csr')
        vector._shape = (1, vector._shape[1] + count)

    def p(self):
        print "Xp: ", self.Xp.toarray()
        print "Xn: ", self.Xn.toarray()
        print "p_weights: ", self.p_weights.toarray()
        print "n_weights: ", self.n_weights.toarray()
        print "(Xp, Xp): ", self.Xp_Xp
        print "(Xn, Xn): ", self.Xn_Xn
        print "(Xn, Xp): ", self.Xn_Xp

    def _dot(self, lhs, rhs):
        return lhs.dot(rhs.transpose())[0, 0]


if __name__ == '__main__':
    clf = HammerSVM()
    X = scipy.sparse.csr_matrix([[-1, 0], [2, 1], [0, -1],
                                 [1, 0], [0, 1], [2, -1]])
    y = np.array([1, -1, 1, -1, 1, -1])

    # two points from different classes for init
    clf.partial_fit(X[:2], y[:2])

    clf.partial_fit(X[2:], y[2:])
    X_pred = scipy.sparse.csr_matrix([[0, 0], [0.5, 0], [1, 0], [0.5, 0.5]])

    print "Decision function: {}".format(clf.decision_function(X_pred))
    print "Predictions (points on "\
          "the boundary are negative) {}".format(clf.predict(X_pred))
    print "Probabilities {}".format(clf.predict_proba(X_pred))
